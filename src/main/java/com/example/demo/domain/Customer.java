package com.example.demo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(value ="Customer" )
public class Customer {
    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
